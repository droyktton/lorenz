#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/generate.h>
#include <thrust/sort.h>
#include <thrust/copy.h>
#include <algorithm>
#include <cstdlib>

float sigma, rho, beta;
bool firstlaunch;
float increment;
const unsigned iter=1;
const float pasodetiempo=0.01;

///// LORENTZ //////
struct lorentz
{
    unsigned int width;
    float sigma, rho, beta;
    float twoPi;
    float dt;   
    float factor; 
    lorentz(unsigned int _width, float _sigma, float _rho, float _beta ){
        width=_width;
        sigma=_sigma;
        rho=_rho;
        beta=_beta;
        twoPi=2.0*M_PI;
        dt=pasodetiempo;
        factor=50;
        //factor=1;
    }
    __host__ __device__
    float4 operator()(float4 res0)
    {
        float4 res1;

        res0.x=res0.x*factor;
        res0.y=res0.y*factor;
        res0.z=res0.z*factor;

        // ecuaciones
        res1.x= res0.x + sigma*(res0.y-res0.x)*dt;
        res1.y= res0.y + (rho*res0.x-res0.y-res0.x*res0.z)*dt;
        res1.z= res0.z + (res0.x*res0.y-beta*res0.z)*dt;
        res1.w = 1.0;

        /*
        float mu=sigma;
        float gamma=rho;
        float S=res0.x;
        float I=res0.y;
        float R=res0.z;
        res1.x= S + (mu-mu*S-beta*S*I)*dt;
        res1.y= I + (beta*S*I-(gamma+mu)*I)*dt;
        res1.z= R + (gamma*I-mu*R)*dt;
        res1.w = 1.0;
        */


        res1.x=res1.x/factor;
        res1.y=res1.y/factor;
        res1.z=res1.z/factor;



        return res1;
    }
};
/*
struct lorentz_initial
{
    unsigned int width;
    lorentz_initial(unsigned int _width)
    {
        width=_width;
    }

    __host__ __device__
    float4 operator()(const float4 res0, const int index)
    {
        //int i=int(index/width);
        //int j=index%width;

        float4 res1;
        res1.x = 0.1; //i*2.0/width-1.0;
        res1.y = 0.1; //j*2.0/width-1.0;
        res1.z = 0.1;
        res1.w = 1.0;
        return res1;
    }
};*/
